package com.example.calc_brad_wilfong

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btn_One.setBackgroundColor(resources.getColor(R.color.buttonPink))
        btn_Two.setBackgroundColor(resources.getColor(R.color.buttonPink))
        btn_Three.setBackgroundColor(resources.getColor(R.color.buttonPink))
        btn_Four.setBackgroundColor(resources.getColor(R.color.buttonPink))
        btn_Five.setBackgroundColor(resources.getColor(R.color.buttonPink))
        btn_Six.setBackgroundColor(resources.getColor(R.color.buttonPink))
        btn_Seven.setBackgroundColor(resources.getColor(R.color.buttonPink))
        btn_Eight.setBackgroundColor(resources.getColor(R.color.buttonPink))
        btn_Nine.setBackgroundColor(resources.getColor(R.color.buttonPink))
        btn_Zero.setBackgroundColor(resources.getColor(R.color.buttonPink))
        btn_Decimal.setBackgroundColor(resources.getColor(R.color.buttonPink))
        btn_Clear.setBackgroundColor(resources.getColor(R.color.buttonPink))

        btn_Add.setBackgroundColor(resources.getColor(R.color.buttonBlue))
        btn_Subtract.setBackgroundColor(resources.getColor(R.color.buttonBlue))
        btn_Mult.setBackgroundColor(resources.getColor(R.color.buttonBlue))
        btn_Divide.setBackgroundColor(resources.getColor(R.color.buttonBlue))
        btn_Equals.setBackgroundColor(resources.getColor(R.color.buttonBlue))

        var calTotal : Float
        var check = false
        var operator = ""
        var one = "0"
        var two = "0"

        btn_One.setOnClickListener{
            buttonOneClick()
            if(!check){
                one += "1"
            }
            else{
                two += "1"
            }
        }
        btn_Two.setOnClickListener{
            buttonTwoClick()
            if(!check){
                one += "2"
            }
            else{
                two += "2"
            }

        }
        btn_Three.setOnClickListener{
            buttonThreeClick()
            if(!check){
                one += "3"
            }
            else{
                two += "3"
            }
        }
        btn_Four.setOnClickListener{
            buttonFourClick()
            if(!check){
                one += "4"
            }
            else{
                two += "4"
            }
        }
        btn_Five.setOnClickListener{
            buttonFiveClick()
            if(!check){
                one += "5"
            }
            else{
                two += "5"
            }
        }
        btn_Six.setOnClickListener{
            buttonSixClick()
            if(!check){
                one += "6"
            }
            else{
                two += "6"
            }
        }
        btn_Seven.setOnClickListener{
            buttonSevenClick()
            if(!check){
                one += "7"
            }
            else{
                two += "7"
            }
        }
        btn_Eight.setOnClickListener{
            buttonEightClick()
            if(!check){
                one += "8"
            }
            else{
                two += "8"
            }
        }
        btn_Nine.setOnClickListener{
            buttonNineClick()
            if(!check){
                one += "9"
            }
            else{
                two += "9"
            }
        }
        btn_Zero.setOnClickListener{
            buttonZeroClick()
            if(!check){
                one += "0"
            }
            else{
                two += "0"
            }
        }


        btn_Equals.setOnClickListener{
            try{
                calTotal = when(operator){
                    "+" -> one.toFloat() + two.toFloat()
                    "-" -> one.toFloat() - two.toFloat()
                    "*" -> one.toFloat() * two.toFloat()
                    "/" -> one.toFloat() / two.toFloat()
                    else -> throw Exception()
                }

               if(calTotal.toDouble() % 1 == 0.0){
                    result_tv.text = calTotal.toInt().toString()
                }
                else {
                    result_tv.text = calTotal.toString()
                }
                //result_tv.text = calTotal.toString()
                one = calTotal.toString()
                two = "0"
                calTotal *= 0
                check = false
                operator = ""

            }catch(e: Exception){
                result_tv.text = resources.getString(R.string.error_string)
            }


        }
        btn_Add.setOnClickListener{
            if(!check){
                buttonAddClick()
                check = true
                operator = "+"
            }
        }
        btn_Subtract.setOnClickListener{
            if(!check) {
                buttonSubClick()
                check = true
                operator = "-"
            }
        }
        btn_Mult.setOnClickListener{
            if(!check){
                buttonMultClick()
                check = true
                operator = "*"
            }
        }
        btn_Divide.setOnClickListener{
            if(!check){
                buttonDivideClick()
                check = true
                operator = "/"
            }
        }

        btn_Clear.setOnClickListener{
            check = false
            result_tv.text = ""
            operator = ""
            one = ""
            two = ""
        }

        btn_Decimal.setOnClickListener{
            buttonDecClick()
            if(!check){
                one += "."
            }
            else{
                two += "."
            }
        }

    }

    private fun buttonOneClick() {
        result_tv.append('1'.toString())
    }

    private fun buttonTwoClick() {
        result_tv.append('2'.toString())
    }

    private fun buttonThreeClick() {
        result_tv.append('3'.toString())
    }

    private fun buttonFourClick() {
        result_tv.append('4'.toString())
    }

    private fun buttonFiveClick() {
        result_tv.append('5'.toString())
    }

    private fun buttonSixClick() {
        result_tv.append('6'.toString())
    }

    private fun buttonSevenClick() {
        result_tv.append('7'.toString())
    }

    private fun buttonEightClick() {
        result_tv.append('8'.toString())
    }

    private fun buttonNineClick() {
        result_tv.append('9'.toString())
    }

    private fun buttonZeroClick() {
        result_tv.append('0'.toString())
    }
    private fun buttonAddClick() {
        result_tv.append('+'.toString())
    }
    private fun buttonSubClick() {
        result_tv.append('-'.toString())
    }

    private fun buttonMultClick() {
        result_tv.append('*'.toString())
    }

    private fun buttonDivideClick() {
        result_tv.append('/'.toString())
    }
    private fun buttonDecClick() {
        result_tv.append('.'.toString())
    }

}
